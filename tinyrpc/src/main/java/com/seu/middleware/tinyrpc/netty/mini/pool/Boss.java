package com.seu.middleware.tinyrpc.netty.mini.pool;

import java.nio.channels.ServerSocketChannel;

/**
 * boss接口
 *
 * @author liangfeihu
 */
public interface Boss {

    /**
     * 加入一个新的ServerSocket
     *
     * @param serverChannel
     */
    void registerAcceptChannelTask(ServerSocketChannel serverChannel);

}
