package com.seu.middleware.tinyrpc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot启动类
 *
 * @author liangfeihu
 * @since 2019/5/5 14:15
 */
@SpringBootApplication
public class TinyRpcApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinyRpcApplication.class, args);
    }

}
