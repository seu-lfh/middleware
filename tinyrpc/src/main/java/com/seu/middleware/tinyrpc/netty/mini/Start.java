package com.seu.middleware.tinyrpc.netty.mini;

import com.seu.middleware.tinyrpc.netty.mini.pool.NioSelectorRunnablePool;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

/**
 * mini netty 启动函数
 *
 * @author liangfeihu
 */
public class Start {

    public static void main(String[] args) {
        // 初始化线程
        NioSelectorRunnablePool nioSelectorRunnablePool = new NioSelectorRunnablePool(Executors.newCachedThreadPool(), Executors.newCachedThreadPool());

        // 获取服务类
        ServerBootstrap bootstrap = new ServerBootstrap(nioSelectorRunnablePool);

        // 绑定端口
        bootstrap.bind(new InetSocketAddress(10101));

        System.out.println("mini netty service start !");
    }

}
