package com.seu.weixin.service;


import com.alibaba.fastjson.JSONObject;
import com.seu.weixin.model.User;

/**
 * 用户业务接口层
 *
 * @author liangfeihu
 * @since 2018/11/8 10:37
 */
public interface IUserService {

    /**
     * 根据用户名获取用户信息，包括从库的地址信息
     *
     * @param userName
     * @return
     */
    User findByName(String userName);

    /**
     * 多数据源查询
     *
     * @return
     */
    JSONObject multiDaoQuery ();

    /**
     * 多数据源插入及异常事务回滚测试
     *
     * @return
     */
    JSONObject multiDaoHandle();

    /**
     * 单数据源插入多条及异常，事务回滚测试
     */
    void whenExceptionWithTx();

    /**
     * 单数据源插入多条及异常，不回滚
     */
    void whenExceptionNoTx();

}
