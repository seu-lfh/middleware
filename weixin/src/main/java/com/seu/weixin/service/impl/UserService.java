package com.seu.weixin.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.seu.weixin.dao.cluster.ClusterBaseDao;
import com.seu.weixin.dao.cluster.IClusterBaseService;
import com.seu.weixin.dao.master.IMasterBaseService;
import com.seu.weixin.dao.master.MasterBaseDao;
import com.seu.weixin.mapper.cluster.CityMapper;
import com.seu.weixin.mapper.master.UserMapper;
import com.seu.weixin.model.City;
import com.seu.weixin.model.User;
import com.seu.weixin.service.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户业务实现层
 *
 * @author liangfeihu
 * @since 2018/11/06 19:22
 */
@Slf4j
@Service
@SuppressWarnings("ALL")
public class UserService implements IUserService {

    /**
     * 主数据源
     */
    @Autowired
    private UserMapper userDao;
    @Resource
    MasterBaseDao masterBaseDao;
    @Autowired
    IMasterBaseService masterBaseService;

    /**
     * 从数据源
     */
    @Autowired
    private CityMapper cityDao;
    @Resource
    ClusterBaseDao clusterBaseDao;
    @Autowired
    IClusterBaseService clusterBaseService;

    @Override
    public User findByName(String userName) {
        User user = userDao.findByName(userName);
        City city = cityDao.findByName("上海");
        log.info("[user]{}", JSONObject.toJSONString(user));
        log.info("[city]{}", JSONObject.toJSONString(city));
        return user;
    }

    /**
     * 多数据源查询
     *
     * @return
     */
    @Override
    public JSONObject multiDaoQuery() {
        JSONObject object = new JSONObject();
        List<User> users = masterBaseDao.listQueryBySQL("where id = #{0}", User.class, 1L);
        List<City> cities = clusterBaseDao.listQueryBySQL("where  city_name=#{0}", City.class, "上海");

        object.put("users", users);
        object.put("cities", cities);

        return object;
    }

    /**
     * 多数据源事务不正常
     * 没有完全回滚
     *
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public JSONObject multiDaoHandle() {
        User user = new User();
        user.setUserName("雪山飞狐2");
        user.setDescription("西域雪山高手2");
        //masterBaseService.insert(user);
        masterBaseDao.insert(user);

        // Integer.parseInt("hello");

        City city = new City();
        city.setCityName("南京2");
        city.setDescription("中山陵很壮观2");
        city.setProvinceId(3L);
        //clusterBaseService.insert(city);
        clusterBaseDao.insert(city);

        Integer.parseInt("hello");

        JSONObject object = new JSONObject();
        object.put("user", user);
        object.put("city", city);

        return object;
    }

    /**
     * 单数据源插入多条及异常，事务回滚测试
     */
    @Transactional(value = "masterTransactionManager", rollbackFor = Exception.class)
    @Override
    public void whenExceptionWithTx() {
        User user = new User();
        user.setUserName("乔峰1");
        user.setDescription("降龙十八掌1");
        masterBaseDao.insert(user);

        Integer.parseInt("xxx");

        User user2 = new User();
        user2.setUserName("乔峰2");
        user2.setDescription("降龙十八掌2");
        masterBaseDao.insert(user2);
    }

    /**
     * 单数据源插入多条及异常，不回滚
     */
    @Override
    public void whenExceptionNoTx() {
        User user = new User();
        user.setUserName("乔峰1");
        user.setDescription("降龙十八掌1");
        masterBaseDao.insert(user);

        Integer.parseInt("xxx");

        User user2 = new User();
        user2.setUserName("乔峰2");
        user2.setDescription("降龙十八掌2");
        masterBaseDao.insert(user2);
    }
}
