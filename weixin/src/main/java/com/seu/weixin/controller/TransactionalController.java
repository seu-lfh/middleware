package com.seu.weixin.controller;

import com.alibaba.fastjson.JSONObject;
import com.seu.weixin.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 事务(回滚)测试
 *
 * @author liangfeihu
 * @since 2018/11/8 17:20
 */
@Slf4j
@RestController
public class TransactionalController {

    @Resource
    UserService userService;

    @GetMapping("/user/tx")
    public ResponseEntity userDaoTx() {
        JSONObject user = null;
        try {
            user = userService.multiDaoHandle();
        } catch (Exception e) {
            log.error("[userDaoTx]出错了", e);
            return ResponseEntity.ok("出错了");
        }
        log.info("[userDaoTx json]{}", JSONObject.toJSONString(user));
        return ResponseEntity.ok(user);
    }

    @GetMapping("/user/withtx")
    public ResponseEntity userDaoWithTx() {
        try {
            userService.whenExceptionWithTx();
        } catch (Exception e) {
            log.error("[userDaoWithTx]出错了", e);
        }
        return ResponseEntity.ok("OK");
    }

    @GetMapping("/user/notx")
    public ResponseEntity userDaoNoTx() {
        try {
            userService.whenExceptionNoTx();
        } catch (Exception e) {
            log.error("[userDaoNoTx]出错了", e);
        }
        return ResponseEntity.ok("OK");
    }
}
