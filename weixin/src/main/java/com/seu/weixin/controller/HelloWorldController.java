package com.seu.weixin.controller;

import com.alibaba.fastjson.JSONObject;
import com.seu.weixin.model.User;
import com.seu.weixin.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * mybatis模块测试
 *
 * @author liangfeihu
 * @since 2018/11/7 17:20
 */
@Slf4j
@RestController
public class HelloWorldController {

    @Resource
    UserService userService;

    @RequestMapping("/")
    public String sayHello() {
        return "Hello,World!";
    }

    @GetMapping("/user/xml")
    public ResponseEntity user() {
        User user = userService.findByName("liangfeihu");
        log.info("[user json]{}", JSONObject.toJSONString(user));
        return ResponseEntity.ok(user);
    }

    @GetMapping("/user/dao")
    public ResponseEntity userDao() {
        JSONObject user = userService.multiDaoQuery();
        log.info("[userDao json]{}", JSONObject.toJSONString(user));
        return ResponseEntity.ok(user);
    }
}
