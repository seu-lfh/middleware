package com.seu.weixin.dao.master;

import com.seu.middleware.mybatis.GenericDao;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * @author liangfeihu
 * @since 2018/11/7
 */
@Repository
public class MasterBaseDao extends GenericDao{
    @Autowired
    @Qualifier(value = "masterSqlSession")
    SqlSession masterSqlSession;

    @Override
    protected SqlSession getSession() {
        return masterSqlSession;
    }

    @Override
    public String getBasePackage() {
        return "com.seu.weixin.mapper.master";
    }

}
