package com.seu.weixin.dao.master;

import com.seu.middleware.mybatis.GenericDao;
import com.seu.middleware.mybatis.GenericService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author liangfeihu
 * @since 2018/11/7
 */
@Service
@Transactional(value = "masterTransactionManager", rollbackFor = Exception.class)
public class MasterBaseService extends GenericService implements IMasterBaseService {
   @Resource
   MasterBaseDao masterBaseDao;

    @Override
    public GenericDao getGenericDao() {
        return masterBaseDao;
    }
}
