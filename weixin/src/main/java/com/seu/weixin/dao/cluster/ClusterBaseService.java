package com.seu.weixin.dao.cluster;

import com.seu.middleware.mybatis.GenericDao;
import com.seu.middleware.mybatis.GenericService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author liangfeihu
 * @since 2018/11/7
 */
@Service
@Transactional(value = "clusterTransactionManager", rollbackFor = Exception.class)
public class ClusterBaseService extends GenericService implements IClusterBaseService {
    @Resource
    ClusterBaseDao clusterBaseDao;

    @Override
    public GenericDao getGenericDao() {
        return clusterBaseDao;
    }
}
