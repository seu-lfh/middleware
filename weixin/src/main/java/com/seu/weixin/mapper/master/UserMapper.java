package com.seu.weixin.mapper.master;

import com.seu.middleware.mybatis.GenericMapper;
import com.seu.weixin.model.User;
import org.apache.ibatis.annotations.Param;

/**
 * 用户 Mapper 接口类
/**
 * @author liangfeihu
 * @since 2018/11/7 17:27
 */
public interface UserMapper extends GenericMapper<User> {

    /**
     * 根据用户名获取用户信息
     *
     * @param userName
     * @return
     */
    User findByName(@Param("userName") String userName);
}
