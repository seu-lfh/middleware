package com.seu.weixin.mapper.cluster;

import com.seu.middleware.mybatis.GenericMapper;
import com.seu.weixin.model.City;
import org.apache.ibatis.annotations.Param;

/**
 * 城市 DAO 接口类
 *
 * @author liangfeihu
 * @since 2018/11/7 17:28
 */
public interface CityMapper extends GenericMapper<City> {

    /**
     * 根据城市名称，查询城市信息
     *
     * @param cityName 城市名
     */
    City findByName(@Param("cityName") String cityName);

}
