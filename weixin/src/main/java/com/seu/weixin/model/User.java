package com.seu.weixin.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 用户实体类
 *
 * @author liangfeihu
 * @since 2018/11/7 17:26
 */
@Table(name = "user")
@Data
@NoArgsConstructor
@AllArgsConstructor
//@NameStyle(Style.normal)
//@JsonIgnoreProperties
public class User implements Serializable {
    private static final long serialVersionUID = -6240585293909400691L;

    /**
     * 城市编号
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private Long id;

    /**
     * 城市名称
     */
    private String userName;

    /**
     * 描述
     */
    private String description;

}
