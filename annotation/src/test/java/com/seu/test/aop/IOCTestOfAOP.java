package com.seu.test.aop;

import com.seu.aop.MathCalculator;
import com.seu.beans.Boss;
import com.seu.beans.Car;
import com.seu.beans.Color;
import com.seu.config.MainConfigOfAOP;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.junit.Test;

public class IOCTestOfAOP {
    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfAOP.class);

    @Test
    public void test01() {

        //1、不要自己创建对象
//		MathCalculator mathCalculator = new MathCalculator();
//		mathCalculator.div(1, 1);

        MathCalculator mathCalculator = applicationContext.getBean(MathCalculator.class);
        mathCalculator.div(1, 2);

        //printBeanNames();

        Car car = applicationContext.getBean(Car.class);
        System.out.println(car);

        Color color = applicationContext.getBean(Color.class);
        System.out.println(color);

        Boss boss = applicationContext.getBean(Boss.class);
        System.out.println(boss);

        applicationContext.close();
    }

    private void printBeanNames() {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        System.out.println("---------------bean names in IOC start-----------------");
        for (String name : beanDefinitionNames) {
            System.out.println(name);
        }
        System.out.println("---------------bean names in IOC end-----------------");
    }

}
