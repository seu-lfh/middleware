package com.seu.test.tx;

import com.seu.config.MainConfigOfTx;
import com.seu.tx.UserService;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTestOfTx {

    @Test
    public void test01() {
        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(MainConfigOfTx.class);

        UserService userService = applicationContext.getBean(UserService.class);

        // SQL操作
        userService.insertUser();

        applicationContext.close();
    }

}
