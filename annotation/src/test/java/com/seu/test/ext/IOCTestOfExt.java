package com.seu.test.ext;

import com.seu.config.MainConfigOfExt;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTestOfExt {

    @Test
    public void test01() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfExt.class);

        printBeanNames(applicationContext);

        //发布事件；
        applicationContext.publishEvent(new ApplicationEvent(new String("我发布的事件")) {
        });

        applicationContext.close();
    }

    private void printBeanNames(ApplicationContext applicationContext) {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        System.out.println("---------------bean names in IOC start-----------------");
        for (String name : beanDefinitionNames) {
            System.out.println(name);
        }
        System.out.println("---------------bean names in IOC end-----------------");
    }

}
