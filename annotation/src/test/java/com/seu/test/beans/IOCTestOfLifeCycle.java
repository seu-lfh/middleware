package com.seu.test.beans;

import com.seu.config.MainConfigOfLifeCycle;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTestOfLifeCycle {

    @Test
    public void test01() {
        //1、创建ioc容器
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfLifeCycle.class);
        System.out.println("容器创建完成...");

        printBeanNames(applicationContext);

        applicationContext.getBean("car");

        //关闭容器
        applicationContext.close();
    }

    private static void printBeanNames(ApplicationContext applicationContext) {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        System.out.println("---------------bean names in IOC start-----------------");
        for (String name : beanDefinitionNames) {
            System.out.println(name);
        }
        System.out.println("---------------bean names in IOC end-----------------");
    }
}
