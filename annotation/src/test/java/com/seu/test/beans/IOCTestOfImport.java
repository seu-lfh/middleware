package com.seu.test.beans;

import com.seu.beans.Person;
import com.seu.config.MainConfigOfImport;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class IOCTestOfImport {

    @SuppressWarnings("resource")
    public static void main(String[] args) {
//		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("beans.xml");
//		Person bean = (Person) applicationContext.getBean("person");
//		System.out.println(bean);

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConfigOfImport.class);

        printBeanNames(applicationContext);

        Object bean = applicationContext.getBean("person");
        System.out.println(bean);

        String[] namesForType = applicationContext.getBeanNamesForType(Person.class);
        for (String name : namesForType) {
            System.out.println(name);
        }

    }

    private static void printBeanNames(ApplicationContext applicationContext) {
        String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        System.out.println("---------------bean names in IOC start-----------------");
        for (String name : beanDefinitionNames) {
            System.out.println(name);
        }
        System.out.println("---------------bean names in IOC end-----------------");
    }

}
