package com.seu.test.beans;

import com.seu.beans.Boss;
import com.seu.beans.Car;
import com.seu.beans.Color;
import com.seu.config.MainConifgOfAutowired;
import com.seu.service.BookService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.junit.Test;

public class IOCTestOfAutowired {

    @Test
    public void test01() {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(MainConifgOfAutowired.class);

        BookService bookService = applicationContext.getBean(BookService.class);
        System.out.println(bookService);

        //BookDao bean = applicationContext.getBean(BookDao.class);
        //System.out.println(bean);

        Boss boss = applicationContext.getBean(Boss.class);
        System.out.println(boss);

        Car car = applicationContext.getBean(Car.class);
        System.out.println(car);

        Color color = applicationContext.getBean(Color.class);
        System.out.println(color);

        System.out.println(applicationContext);
        applicationContext.close();
    }

}
