package com.seu.service;


import com.seu.dao.BookDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service
public class BookService {

    @Qualifier("bookDao2")
    //@Autowired(required=false)
    @Autowired
    //@Resource(name="bookDao")
    //@Inject
    private BookDao bookDao;

    public void print() {
        System.out.println(bookDao);
    }

    @Override
    public String toString() {
        return "BookService [bookDao=" + bookDao + "]";
    }


}
