package com.seu.config;

import com.seu.beans.Person;
import com.seu.condition.MyTypeFilter;
import com.seu.service.BookService;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;



/*@ComponentScans(
		value = {
				@ComponentScan(value="com.seu",includeFilters = {
						@Filter(type=FilterType.ANNOTATION,classes={Controller.class}),
						@Filter(type=FilterType.ASSIGNABLE_TYPE,classes={BookService.class}),
						@Filter(type= FilterType.CUSTOM,classes={MyTypeFilter.class})
				},useDefaultFilters = false)
		}
)*/
//@ComponentScan  value:指定要扫描的包
//excludeFilters = Filter[] ：指定扫描的时候按照什么规则排除那些组件
//includeFilters = Filter[] ：指定扫描的时候只需要包含哪些组件
//FilterType.ANNOTATION：按照注解
//FilterType.ASSIGNABLE_TYPE：按照给定的类型；
//FilterType.ASPECTJ：使用ASPECTJ表达式
//FilterType.REGEX：使用正则指定
//FilterType.CUSTOM：使用自定义规则

// @Configuration:配置类==配置文件, 告诉Spring这是一个配置类
// @PropertySource:注意:前后不要有空格
// 使用@PropertySource读取外部配置文件中的k/v保存到运行的环境变量中;加载完外部的配置文件以后使用${}取出配置文件的值

/**
 * Spring Beans注解配置
 *
 * @author liangfeihu
 * @since 2019/1/11 16:41
 */
@ComponentScan(
        value = {"com.seu.beans", "com.seu.dao", "com.seu.service", "com.seu.controller"},
        includeFilters = { // 或的关系,满足三个条件之一即可
                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Component.class}),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {BookService.class}),
                @ComponentScan.Filter(type = FilterType.CUSTOM, classes = {MyTypeFilter.class})
        },
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ANNOTATION, classes = {Controller.class})
        },
        // 不使用默认的过滤器指定的规则
        useDefaultFilters = false)
@PropertySource("classpath:/person.properties")
@Configuration
public class MainConfigOfBeans {

    //给容器中注册一个Bean;类型为返回值的类型，id默认是用方法名作为id

    @Bean
    public Person person01() {
        return new Person("lisi", 20);
    }

    @Bean("person")
    public Person person() {
        return new Person("王五", 26);
    }

}
