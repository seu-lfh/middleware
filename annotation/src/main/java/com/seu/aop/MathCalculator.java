package com.seu.aop;

/**
 * 一个除法计算器
 *
 * @author liangfeihu
 * @since 2019/1/14 16:27
 */
public class MathCalculator {

    public int div(int i, int j) {
        System.out.println("MathCalculator...div...");
        return i / j;
    }

}
