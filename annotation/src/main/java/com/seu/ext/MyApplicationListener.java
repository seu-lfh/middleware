package com.seu.ext;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Spring中事件监听
 *
 * @author liangfeihu
 * @since 2019/1/16 14:34
 */
@Component
public class MyApplicationListener implements ApplicationListener<ApplicationEvent> {

    //当容器中发布此事件以后，方法触发

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        System.out.println("[MyApplicationListener]收到事件：" + event);
    }

}
