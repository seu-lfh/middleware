package com.seu.ext;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

/**
 * 使用注解监听事件
 *
 * @author liangfeihu
 * @since 2019/1/16 15:53
 */
@Service
public class EventListenerService {

    @EventListener(classes = {ApplicationEvent.class})
    public void listen(ApplicationEvent event) {
        System.out.println("[EventListenerService]。。。监听到的事件：" + event);
    }

}
