package com.seu.beans;

/**
 * @author liangfeihu
 * @since 2019/1/11
 */
public class Color {

    //@Autowired 不起作用
    private Car car;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    @Override
    public String toString() {
        return "Color [car=" + car + "]";
    }
}
