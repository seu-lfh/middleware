package com.seu.beans.ext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * 通过注解@PostConstruct指定初始化方法,
 * 通过注解@PreDestroy指定销毁方法
 *
 * @author liangfeihu
 * @since 2019/1/16 10:20
 */
@Component
public class Dog implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    public Dog() {
        System.out.println("dog constructor...");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }


    //对象创建并赋值之后调用

    @PostConstruct
    public void init() {
        System.out.println("Dog....@PostConstruct...");
    }

    //容器移除对象之前

    @PreDestroy
    public void detory() {
        System.out.println("Dog....@PreDestroy...");
    }


}
