package com.seu.beans;

/**
 * @author liangfeihu
 * @since 2019/1/11
 */
public class Blue {

    public Blue() {
        System.out.println("blue...constructor");
    }

    public void init() {
        System.out.println("blue...init...");
    }

    public void destroy() {
        System.out.println("blue...destroy...");
    }
}
