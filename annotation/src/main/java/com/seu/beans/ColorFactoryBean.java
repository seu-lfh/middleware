package com.seu.beans;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 创建一个Spring定义的FactoryBean
 *
 * @author liangfeihu
 * @since 2019/1/11
 */
@Component
public class ColorFactoryBean implements FactoryBean<Color> {
    @Autowired
    private Car car;

    //返回一个Color对象，这个对象会添加到容器中

    @Override
    public Color getObject() throws Exception {
        System.out.println("ColorFactoryBean...getObject...");
        Color color = new Color();
        color.setCar(car);
        return color;
    }

    @Override
    public Class<?> getObjectType() {
        return Color.class;
    }

    //是单例？
    //true：这个bean是单实例，在容器中保存一份
    //false：多实例，每次获取都会创建一个新的bean；

    @Override
    public boolean isSingleton() {
        return true;
    }

}
