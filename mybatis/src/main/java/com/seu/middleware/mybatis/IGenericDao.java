package com.seu.middleware.mybatis;

/**
 * Created by liangfeihu on 2016/11/14.
 */
public interface IGenericDao {

    default String getBasePackage() {
        return null;
    }

    default String getInterface() {
        return "com.seu.middleware.mybatis.GenericMapper";
    }

    default String getMapperFullClassName(Class modelClass) {
        return null;
    }
}
