package com.seu.middleware.mybatis.mapper;

import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.annotation.RegisterMapper;

@RegisterMapper
public interface UpdateSQLMapper<T> {
    @UpdateProvider(
            type = UpdateSQLProvider.class,
            method = "dynamicSQL"
    )
    int update(T model);
}
