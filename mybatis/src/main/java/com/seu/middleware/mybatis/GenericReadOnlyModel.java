package com.seu.middleware.mybatis;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

/**
 * @author liangfeihu
 * @since 2017/11/7 16:26
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GenericReadOnlyModel implements Serializable {
    private static final long serialVersionUID = -6240585293909400691L;
    @Id
    @GeneratedValue(generator = "JDBC")
    protected Long id;

    protected Date created;

}
