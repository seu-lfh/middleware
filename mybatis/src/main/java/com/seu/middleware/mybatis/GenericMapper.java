package com.seu.middleware.mybatis;

import com.seu.middleware.mybatis.mapper.SelectSQLMapper;
import com.seu.middleware.mybatis.mapper.SingleByConditionMapper;
import com.seu.middleware.mybatis.mapper.UpdateSQLMapper;
import tk.mybatis.mapper.annotation.RegisterMapper;
import tk.mybatis.mapper.common.Marker;
import tk.mybatis.mapper.common.base.delete.DeleteByPrimaryKeyMapper;
import tk.mybatis.mapper.common.base.insert.InsertMapper;
import tk.mybatis.mapper.common.base.select.SelectByPrimaryKeyMapper;
import tk.mybatis.mapper.common.base.select.SelectCountMapper;
import tk.mybatis.mapper.common.base.select.SelectMapper;
import tk.mybatis.mapper.common.base.select.SelectOneMapper;
import tk.mybatis.mapper.common.base.update.UpdateByPrimaryKeyMapper;
import tk.mybatis.mapper.common.condition.*;
import tk.mybatis.mapper.common.rowbounds.SelectByConditionRowBoundsMapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

/**
 * Created by liangfeihu on 2016/6/16.
 */
@RegisterMapper
public interface GenericMapper<T> extends
        InsertMapper<T>,
        InsertListMapper<T>,
        SelectByPrimaryKeyMapper<T>,
        SelectMapper<T>,
        SelectOneMapper<T>,
        SelectCountMapper<T>,
        UpdateSQLMapper<T>,
        UpdateByPrimaryKeyMapper<T>,
        UpdateByConditionMapper<T>,
        UpdateByConditionSelectiveMapper<T>,
        DeleteByPrimaryKeyMapper<T>,
        DeleteByConditionMapper<T>,
        SelectByConditionMapper<T>,
        SelectByConditionRowBoundsMapper<T>,
        SelectCountByConditionMapper<T>,
        SingleByConditionMapper<T>,
        SelectSQLMapper<T>,
        Marker {

    public final static String className = "com.seu.middleware.mybatis.GenericMapper";

}
