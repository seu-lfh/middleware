package com.seu.generator.dao;

import java.util.List;
import java.util.Map;

/**
 * 代码生成器
 *
 * @author liangfeihu
 * @since 2018/11/26 14:12
 */
public interface SysGeneratorDao {
	
	List<Map<String, Object>> queryList(Map<String, Object> map);
	
	int queryTotal(Map<String, Object> map);
	
	Map<String, String> queryTable(String tableName);
	
	List<Map<String, String>> queryColumns(String tableName);
}
